from django.shortcuts import render
from django.http import HttpResponse
from .models import People
# Create your views here.


def index(request):
    ctx  = {}
    ctx['all_people'] = People.objects.all()
    return render(request, 'index.html', ctx)